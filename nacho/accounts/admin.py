# Copyright 2018 Nacho Developers
# See the COPYRIGHT file at the top-level directory of this distribution
#
# This file is part of Nacho. It is subject to the license terms in the LICENSE
# file found in the top-level directory of this distribution. No part of Nacho,
# including this file, may be copied, modified, propagated, or distributed
# except according to the terms contained in the LICENSE file.
#


from django import forms
from django.contrib import admin
from django.contrib.admin.widgets import FilteredSelectMultiple

from .models import LdapUser, LdapTemporaryUser, LdapGroup


class LdapUserAdmin(admin.ModelAdmin):
    exclude = ['dn', 'password', 'jpegPhoto']
    list_display = ['username', 'sn', 'cn', 'mail']
    search_fields = ['sn', 'cn', 'mail', 'username']


class LdapTemporaryUserAdmin(admin.ModelAdmin):
    exclude = ['dn', 'password', 'jpegPhoto']
    list_display = ['username', 'mail']
    search_fields = ['mail', 'username']


class LdapGroupForm(forms.ModelForm):
    memberUid = forms.ModelMultipleChoiceField(
        queryset=LdapUser.objects.all(),
        widget=FilteredSelectMultiple('Users', is_stacked=False),
        required=False,
    )

    class Meta:
        exclude = []
        model = LdapGroup

    def clean_memberUid(self):
        data = self.cleaned_data['memberUid']
        if not data:
            return []
        return list(data.values_list('username', flat=True))


class LdapGroupAdmin(admin.ModelAdmin):
    form = LdapGroupForm
    exclude = ['dn']
    list_display = ['cn', 'gidNumber']
    search_fields = ['cn']


admin.site.register(LdapGroup, LdapGroupAdmin)
admin.site.register(LdapUser, LdapUserAdmin)
admin.site.register(LdapTemporaryUser, LdapTemporaryUserAdmin)
