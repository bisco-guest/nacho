# Copyright 2018 Nacho Developers
# See the COPYRIGHT file at the top-level directory of this distribution
#
# This file is part of Nacho. It is subject to the license terms in the LICENSE
# file found in the top-level directory of this distribution. No part of Nacho,
# including this file, may be copied, modified, propagated, or distributed
# except according to the terms contained in the LICENSE file.
#

import magic
import sys
import smtplib
import ldap

from django.conf import settings
from django.contrib import messages
from django.contrib.auth import logout
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import PasswordChangeView
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.sites.shortcuts import get_current_site
from django.shortcuts import render, redirect
from django.utils.http import urlsafe_base64_decode
from django.urls import reverse_lazy
from django.views import View
from django.views.generic.detail import DetailView
from django.views.generic.edit import UpdateView, FormView

from . import forms as nachoforms
from .tokens import *
from .models import LdapUser, LdapTemporaryUser


def get_url(request):
    protocol = 'https://' if request.is_secure() else 'http://'
    return protocol + str(get_current_site(request))


class RedirectAuthenticatedUserView(View):
    def dispatch(self, request, *args, **kwargs):
        if self.request.user.is_authenticated:
            return redirect('profile')
        return super().dispatch(request, *args, **kwargs)


class LdapTemporaryUserSignup(RedirectAuthenticatedUserView):
    """ Present a signup view for user registration
    """
    template_name = 'accounts/signup_form.html'

    def post(self, request):
        form = nachoforms.LdapTemporaryUserSignupForm(request.POST)
        if form.is_valid():
            try:
                form.save(get_url(request))
                message = 'We have sent you a message with an activation link.'
                messages.info(request, message)
            except smtplib.SMTPException:
                message = 'There was an error sending the email.'
                messages.warning(request, message)
            return render(request, "container.html")

        context = {
            'username_append': settings.NACHO_USERNAME_APPEND,
            'form': form}
        return render(request, self.template_name, context)

    def get(self, request):
        form = nachoforms.LdapTemporaryUserSignupForm()
        context = {
            'username_append': settings.NACHO_USERNAME_APPEND,
            'form': form}
        return render(request, self.template_name, context)


class LdapTokenUserActivate(RedirectAuthenticatedUserView):
    """ This view checks both token and uid and activates
    the account by making an LdapUser object from the
    LdapTemporaryUser object and saving it.
    """

    def get(self, request, token, uidb64):
        username = urlsafe_base64_decode(uidb64).decode()
        try:
            ltu = LdapTemporaryUser.objects.get(username=username)
        except LdapTemporaryUser.DoesNotExist:
            ltu = None

        if (ltu is not None and
                account_activation_token.check_token(ltu, token)):
            lu = LdapUser(
                username=username,
                cn=ltu.cn,
                sn=ltu.sn,
                mail=ltu.mail,
                userPassword=ltu.userPassword)
            lu.save()
            if lu is not None:
                ltu.delete()
                message = 'Account %s activated' % (lu.username)
                messages.success(request, message)
            else:
                message = 'There was an error activating the account.'
                messages.warning(request, message)
            return redirect('login')
        messages.warning(request, "This token is invalid.")
        return render(request, "container.html")


class LdapTokenPasswordReset(FormView):
    """ A view for password resets. It checks the uid
    and the token and presents a password changing form
    """
    def dispatch(self, request, *args, **kwargs):
        if self.request.user.is_authenticated:
            return redirect('profile')
        return super().dispatch(request, *args, **kwargs)

    def get_user(self, uidb64):
        try:
            username = urlsafe_base64_decode(uidb64).decode()
            user = LdapUser.objects.get(username=username)
        except LdapUser.DoesNotExist:
            user = None
        return user

    def get(self, request, token, uidb64):
        ltu = self.get_user(uidb64)

        if (ltu is not None and
                nacho_password_reset_token.check_token(ltu, token)):
            form = nachoforms.LdapPasswordResetForm(ltu)
            return render(request, 'password_reset.html', {'form': form})
        else:
            messages.warning(request, "This token is invalid.")
            return render(request, "container.html")

    def post(self, request, token, uidb64):
        ltu = self.get_user(uidb64)
        if (ltu is not None and
                nacho_password_reset_token.check_token(ltu, token)):
            form = nachoforms.LdapPasswordResetForm(ltu, request.POST)
            if form.is_valid():
                form.save()
                messages.success(request, "Password reset.")
                return redirect('login')
            else:
                return render(request, 'password_reset.html', {'form': form})


class LdapUserDetail(LoginRequiredMixin, DetailView):
    """ Show the details of an LdapUser
    """
    model = LdapUser

    def get_object(self):
        """Return the current users profile."""
        return LdapUser.objects.get(username=self.request.user)


class LdapUserUpdate(LoginRequiredMixin, UpdateView):
    """ Update the attributes of an LdapUser
    and redirect to its profile page
    """
    model = LdapUser
    success_url = reverse_lazy('profile')
    form_class = nachoforms.LdapUserForm

    def post(self, request):
        form = nachoforms.LdapUserForm(request.POST, request.FILES)
        if form.is_valid():
            ldu = self.get_object()
            jpegPhoto = request.FILES.getlist('jpegPhoto')
            if jpegPhoto:
                buf = jpegPhoto[0].file.read()
                if (sys.getsizeof(buf) > settings.NACHO_PROFILE_IMG_MAX_SIZE or
                        magic.from_buffer(buf, mime=True)
                        not in settings.NACHO_PROFILE_IMG_MIMETYPES):
                    message = 'File has to be one of %s'\
                        ' and must be at most %s bytes' % (
                            ",".join(settings.NACHO_PROFILE_IMG_MIMETYPES),
                            settings.NACHO_PROFILE_IMG_MAX_SIZE)
                    messages.warning(request, message)
                else:
                    ldu.jpegPhoto = buf
            ldu.cn = form.cleaned_data['cn']
            ldu.sn = form.cleaned_data['sn']
            ldu.save()
        return redirect('profile')

    def get_object(self):
        """Return the current users object."""
        return LdapUser.objects.get(username=self.request.user)


class LdapPasswordChange(
        LoginRequiredMixin,
        SuccessMessageMixin,
        PasswordChangeView):
    """ A password change view
    """
    template_name = "pass.html"
    form_class = nachoforms.LdapPasswordChangeForm
    success_url = reverse_lazy('profile')
    success_message = "Password changed."


class LdapPasswordResetRequest(View):
    """ Request a password reset link
    """
    template_name = 'password_reset_request.html'

    def dispatch(self, request, *args, **kwargs):
        if self.request.user.is_authenticated:
            return redirect('profile')
        return super().dispatch(request, *args, **kwargs)

    def post(self, request):
        form = nachoforms.LdapPasswordResetRequestForm(request.POST)
        if form.is_valid():
            try:
                form.save(get_url(request))
                messages.success(request, "Password reset email sent.")
            except smtplib.SMTPException:
                message = 'There was an error sending'\
                    'the password reset email.'
                messages.warning(request, message)
            return redirect('login')
        else:
            return render(request, self.template_name, {'form': form})

    def get(self, request):
        form = nachoforms.LdapPasswordResetRequestForm()
        return render(request, self.template_name, {'form': form})


class LdapUserSshkeys(LoginRequiredMixin, DetailView):
    """ Show the details of an LdapUser
    """
    model = LdapUser
    template_name = 'accounts/ldapuser_sshkeys.html'

    def get_object(self):
        """Return the current users profile."""
        return LdapUser.objects.get(username=self.request.user)


class LdapUserSshkeysAdd(LoginRequiredMixin, View):
    def get(self, request):
        form = nachoforms.LdapUserAddSshKeyForm()
        return render(request, 'sshkeys_add.html', {'form': form})

    def post(self, request):
        form = nachoforms.LdapUserAddSshKeyForm(request.POST)
        if form.is_valid():
            try:
                ldu = LdapUser.objects.get(username=self.request.user)
                ldu.sshPublicKey.append(form.cleaned_data['sshPublicKey'])
                ldu.save()
            except ldap.TYPE_OR_VALUE_EXISTS:
                # since v1.2 django-ldapdb doesn't send duplicate
                # values, so this is very unlikely
                message = 'This key already exists.'
                messages.warning(request, message)
            return redirect('sshkeys')
        return render(request, 'sshkeys_add.html', {'form': form})


class LdapUserSshkeysRem(LoginRequiredMixin, View):
    def get(self, request, pk):
        ldapuser = LdapUser.objects.get(username=self.request.user)
        data = {'sshPublicKey': ldapuser.sshPublicKey[pk]}
        form = nachoforms.LdapUserRemSshKeyForm(initial=data)
        return render(request, 'sshkeys_rem.html', {'form': form})

    def post(self, request, pk):
        form = nachoforms.LdapUserRemSshKeyForm(request.POST)
        if form.is_valid():
            ldu = LdapUser.objects.get(username=self.request.user)
            if ldu.sshPublicKey[pk] == form.cleaned_data['sshPublicKey']:
                ldu.sshPublicKey.remove(form.cleaned_data['sshPublicKey'])
            ldu.save()
        return redirect('sshkeys')


class LdapUserEmailUpdate(LoginRequiredMixin, View):

    def get(self, request):
        form = nachoforms.LdapUserEmailUpdate()
        return render(request, 'accounts/email_change.html', {'form': form})

    def post(self, request):
        form = nachoforms.LdapUserEmailUpdate(request.POST)
        if form.is_valid():
            try:
                form.save(self.request.user.username, get_url(request))
                message = 'Email change token sent to '\
                    '%s.' % (form.cleaned_data['mail'])
                messages.success(request, message)
            except smtplib.SMTPException:
                message = 'There was an error sendig a mail to '\
                    '%s.' % (form.cleaned_data['mail'])
                messages.warning(request, message)
        return redirect('profile')


class LdapTokenEmailUpdate(View):
    """ This view checks both token and uid and changes
    the email address
    """
    def get(self, request, token, uidb64):
        username = urlsafe_base64_decode(uidb64).decode()
        ldapuser = LdapUser.objects.get(username=username)
        try:
            ltu = LdapTemporaryUser.objects.get(username=username)
        except LdapTemporaryUser.DoesNotExist:
            ltu = None

        if (ldapuser is not None and ltu is not None and
           email_update_token.check_token(ldapuser, token)):
            ldapuser.mail = ltu.mail
            ldapuser.save()
            ltu.delete()
            message = 'Email address of %s changed to '\
                '%s.' % (ldapuser.username, ldapuser.mail)
            messages.success(request, message)
            return redirect('login')
        messages.warning(request, "This token is invalid.")
        return render(request, "container.html")


class LdapUserDeleteRequest(View):
    """ Request a password reset link
    """
    template_name = 'account_delete_request.html'

    def post(self, request):
        form = nachoforms.LdapAccountDeletionRequestForm(request.POST)
        if form.is_valid(self.request.user.username):
            try:
                form.save(get_url(request))
                messages.success(request, "Account deletion token email sent.")
            except smtplib.SMTPException:
                message = 'There was an error sending'\
                    'the account deletion token email.'
                messages.warning(request, message)
            return redirect('login')
        else:
            return render(request, self.template_name, {'form': form})

    def get(self, request):
        form = nachoforms.LdapAccountDeletionRequestForm()
        return render(request, self.template_name, {'form': form})


class LdapTokenDeleteUser(View):
    def get(self, request, token, uidb64):
        username = urlsafe_base64_decode(uidb64).decode()
        try:
            ldapuser = LdapUser.objects.get(username=username)
        except LdapUser.DoesNotExist:
            messages.warning(request, "This token is invalid.")
            ldapuser = None

        if (ldapuser is not None and
           UserDeleteTokenGenerator().check_token(ldapuser, token)):
            try:
                logout(request)
                ltu = LdapTemporaryUser.objects.get(username=username)
                ltu.delete()
            except LdapTemporaryUser.DoesNotExist:
                ltu = None
            ldapuser.delete()
            messages.warning(request, "Account has been deleted.")

        return render(request, "container.html")
