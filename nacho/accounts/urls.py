# Copyright 2018 Nacho Developers
# See the COPYRIGHT file at the top-level directory of this distribution
#
# This file is part of Nacho. It is subject to the license terms in the LICENSE
# file found in the top-level directory of this distribution. No part of Nacho,
# including this file, may be copied, modified, propagated, or distributed
# except according to the terms contained in the LICENSE file.
#


from django.urls import path, include, re_path
from django.contrib.auth import views as authviews
from . import views

urlpatterns = [
    path('', authviews.LoginView.as_view(redirect_authenticated_user=True),
         name='login'),
    path('login/', authviews.LoginView.as_view(
         redirect_authenticated_user=True), name='login'),
    path('password_change/', views.LdapPasswordChange.as_view(),
         name='password_change'),
    path('password_reset/', views.LdapPasswordResetRequest.as_view(),
         name='password_reset_request'),
    path('signup/', views.LdapTemporaryUserSignup.as_view(),
         name='signup'),
    path('accounts/profile/', views.LdapUserDetail.as_view(),
         name='profile'),
    path('accounts/profile/update/', views.LdapUserUpdate.as_view(),
         name='profile_update'),
    path('accounts/sshkeys/', views.LdapUserSshkeys.as_view(),
         name='sshkeys'),
    path('accounts/sshkeys/add/', views.LdapUserSshkeysAdd.as_view(),
         name='sshkeys_add'),
    path('accounts/sshkeys/rem/<int:pk>/', views.LdapUserSshkeysRem.as_view(),
         name='sshkeys_rem'),
    path('accounts/email/update/', views.LdapUserEmailUpdate.as_view(),
         name='email_update'),
    path('accounts/delete/', views.LdapUserDeleteRequest.as_view(),
         name='deleterequest'),
    re_path('activate/(?P<uidb64>[0-9A-Za-z_\-]+)/'
            '(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/',
            views.LdapTokenUserActivate.as_view(), name='activate'),
    re_path('reset/(?P<uidb64>[0-9A-Za-z_\-]+)/'
            '(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/',
            views.LdapTokenPasswordReset.as_view(), name='reset'),
    re_path('emailupdate/(?P<uidb64>[0-9A-Za-z_\-]+)/'
            '(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/',
            views.LdapTokenEmailUpdate.as_view(), name='email_token'),
    re_path('delete/(?P<uidb64>[0-9A-Za-z_\-]+)/'
            '(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/',
            views.LdapTokenDeleteUser.as_view(), name='delete_token'),
    path('', include('django.contrib.auth.urls')),
]
