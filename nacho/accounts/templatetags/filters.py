# Copyright 2018 Nacho Developers
# See the COPYRIGHT file at the top-level directory of this distribution
#
# This file is part of Nacho. It is subject to the license terms in the LICENSE
# file found in the top-level directory of this distribution. No part of Nacho,
# including this file, may be copied, modified, propagated, or distributed
# except according to the terms contained in the LICENSE file.
#


import textwrap
import base64

from django import template

register = template.Library()


@register.filter(name='addcss')
def addcss(field, css):
    return field.as_widget(attrs={"class": css})


@register.filter(name='wrap')
def wrap(value, pos):
    return '\n'.join(textwrap.wrap(value, pos))


@register.filter(name='b64enc')
def b64enc(value):
    return base64.b64encode(value).decode()
