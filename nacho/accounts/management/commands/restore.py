import ldap
import ldap.modlist as modlist
import logging
import ldif
import sys

from django.core.management.base import BaseCommand
from django.conf import settings

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Restore backend db from LDIF backup'

    def handle(self, *args, **options):
        ldapcon = ldap.initialize(settings.AUTH_LDAP_SERVER_URI)
        parser = ldif.LDIFRecordList(sys.stdin)
        parser.parse()
        try:
            ldapcon.simple_bind_s(
                settings.DATABASES['ldap']['USER'],
                settings.DATABASES['ldap']['PASSWORD'])
        except ldap.LDAPError as e:
            logger.error("handle(): " + str(e))

        for dn, entry in parser.all_records:
            add_modlist = modlist.addModlist(entry)
            try:
                ldapcon.add_s(dn, add_modlist)
            except ldap.LDAPError as e:
                logger.error("handle(): " + str(e))
