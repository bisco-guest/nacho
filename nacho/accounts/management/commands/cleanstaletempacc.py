import ldap
import logging
from datetime import datetime, timedelta

from django.core.management.base import BaseCommand
from nacho.accounts.models import LdapTemporaryUser
from django.conf import settings

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Deletes all temporary accounts older than <n> days.'

    def add_arguments(self, parser):
        parser.add_argument('days', type=int)

    def handle(self, *args, **options):
        # it would be great if that worked...
        # filterstring = 'createTimestamp>={}'.format(self.timestamp())
        filterstring = 'objectClass=*'
        ldapcon = ldap.initialize(settings.AUTH_LDAP_SERVER_URI)
        try:
            ldapcon.simple_bind_s(
                settings.DATABASES['ldap']['USER'],
                settings.DATABASES['ldap']['PASSWORD'])
            results = ldapcon.search_s(
                "ou=temp," + settings.NACHO_BASEDN,
                ldap.SCOPE_ONELEVEL,
                filterstr=filterstring,
                attrlist=['createTimeStamp'])
        except ldap.LDAPError as e:
            logger.error("handle(): " + str(e))

        tobedeleted = list(filter(lambda x: self.checktimestamp(
            x[1]['createTimestamp'][0].decode(),
            options['days']), results))

        for account in tobedeleted:
            ltu = LdapTemporaryUser.objects.get(dn=account[0])
            ltu.delete()

    # def timestamp(self):
    #     d = datetime.today() - timedelta(days=3)
    #     timestamp = "{}{}{}00000Z".format(
    #         d.strftime('%Y'),
    #         d.strftime('%m'),
    #         d.strftime('%d'))
    #     return timestamp

    def checktimestamp(self, timestamp, days):
        d = datetime.today() - timedelta(days=days)
        if datetime.strptime(timestamp[:8], "%Y%m%d") < d:
            return True
        return False
