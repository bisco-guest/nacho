import ldap
import logging
import ldif
import sys

from django.core.management.base import BaseCommand
from django.conf import settings

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Create LDIF backup of the backend db'

    def handle(self, *args, **options):
        ldapcon = ldap.initialize(settings.AUTH_LDAP_SERVER_URI)
        ldif_writer = ldif.LDIFWriter(sys.stdout)
        try:
            ldapcon.simple_bind_s(
                settings.DATABASES['ldap']['USER'],
                settings.DATABASES['ldap']['PASSWORD'])
            results = ldapcon.search_s(
                settings.NACHO_BASEDN,
                ldap.SCOPE_SUBTREE)
            for result in results:
                ldif_writer.unparse(result[0], result[1])
        except ldap.LDAPError as e:
            logger.error("handle(): " + str(e))
