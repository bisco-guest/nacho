# Copyright 2018 Nacho Developers
# See the COPYRIGHT file at the top-level directory of this distribution
#
# This file is part of Nacho. It is subject to the license terms in the LICENSE
# file found in the top-level directory of this distribution. No part of Nacho,
# including this file, may be copied, modified, propagated, or distributed
# except according to the terms contained in the LICENSE file.
#


import sshpubkeys
from sshpubkeys import SSHKey

from django.conf import settings
from django.contrib.auth.forms import (PasswordChangeForm,
                                       UserCreationForm,
                                       SetPasswordForm)
from django.core.mail import send_mail
from django import forms
from django.template.loader import render_to_string
from django.utils.http import urlsafe_base64_encode
from django.utils.encoding import force_bytes

from .tokens import (account_activation_token,
                     nacho_password_reset_token,
                     email_update_token)
from .models import LdapUser, LdapTemporaryUser


class LdapPasswordResetRequestForm(forms.Form):
    username = forms.CharField(label='Username', max_length=100)

    def save(self, currentsite):
        username = self.cleaned_data.get('username')
        try:
            user = LdapUser.objects.get(username=username)
            subject = 'Reset Your Password'
            uidb64 = urlsafe_base64_encode(force_bytes(username)).decode()
            message = render_to_string('email-templates/password_reset.html', {
                'user': username, 'domain': currentsite,
                'username': uidb64,
                'token': nacho_password_reset_token.make_token(user), })
            send_mail(subject, message,
                      settings.DEFAULT_FROM_EMAIL,
                      [user.mail])
        except LdapUser.DoesNotExist:
            pass


class LdapTemporaryUserSignupForm(UserCreationForm):
    class Meta:
        model = LdapTemporaryUser
        fields = ('username', 'cn', 'sn', 'mail', 'password1', 'password2', )

    def save(self, currentsite):
        username = self.cleaned_data.get('username')
        username += settings.NACHO_USERNAME_APPEND
        password = self.cleaned_data.get('password1')
        cn = self.cleaned_data.get('cn')
        sn = self.cleaned_data.get('sn')
        mail = self.cleaned_data.get('mail')
        user = LdapTemporaryUser(username=username, cn=cn, sn=sn, mail=mail)
        user.save()
        user.set_password(password)
        if user:
            subject = 'Activate Your Account'
            uidb64 = urlsafe_base64_encode(force_bytes(username)).decode()
            message = render_to_string(
                'email-templates/account_activation.html', {
                    'user': username, 'domain': currentsite,
                    'username': uidb64,
                    'token': account_activation_token.make_token(user), })
            send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, [mail])

    def is_valid(self):
        valid = super(LdapTemporaryUserSignupForm, self).is_valid()
        if not valid:
            return valid

        if self.cleaned_data.get('username') in settings.NACHO_FORBIDDEN_UIDS:
            self.add_error('username', 'Username is already in use.')
            return False

        user = None
        username = self.cleaned_data.get('username')
        username += settings.NACHO_USERNAME_APPEND
        try:
            user = LdapUser.objects.get(username=username)
        except LdapUser.DoesNotExist:
            pass
        try:
            user = LdapTemporaryUser.objects.get(username=username)
        except LdapTemporaryUser.DoesNotExist:
            pass

        if user is None:
            return True
        self.add_error('username', 'Username is already in use.')
        return False


class LdapPasswordChangeForm(PasswordChangeForm):
    def clean_old_password(self):
        """
        Validate that the old_password field is correct.
        """
        ldapuser = LdapUser.objects.get(username=self.user)
        old_password = self.cleaned_data["old_password"]
        if not ldapuser.check_password(old_password):
            raise forms.ValidationError(
                self.error_messages['password_incorrect'],
                code='password_incorrect',
            )
        return old_password

    def save(self, commit=True):
        """
        Save the new password in ldap
        """
        ldapuser = LdapUser.objects.get(username=self.user)
        old_password = self.cleaned_data["old_password"]
        new_password = self.cleaned_data["new_password1"]
        ldapuser.change_password(old_password, new_password)


class LdapPasswordResetForm(SetPasswordForm):
    def save(self, commit=True):
        """
        Save the new password in ldap
        """
        ldapuser = LdapUser.objects.get(username=self.user)
        new_password = self.cleaned_data["new_password1"]
        ldapuser.set_password(new_password)


class LdapUserForm(forms.ModelForm):
    class Meta:
        model = LdapUser
        fields = ['cn', 'sn', 'jpegPhoto']
        widgets = {
            'jpegPhoto': forms.FileInput(),
        }


class LdapUserAddSshKeyForm(forms.Form):
    sshPublicKey = forms.CharField(widget=forms.Textarea)

    def is_valid(self):
        valid = super(LdapUserAddSshKeyForm, self).is_valid()
        if not valid:
            return valid

        sshPublicKey = self.cleaned_data.get('sshPublicKey')
        sshPublicKey = ' '.join(sshPublicKey.split())

        ssh = SSHKey(sshPublicKey, strict=True, disallow_options=True)
        try:
            ssh.parse()
        except sshpubkeys.InvalidKeyError as err:
            message = "Invalid Key: %s" % (err)
            self.add_error('sshPublicKey', message)
            return False
        except sshpubkeys.NotImplementedError as err:
            message = "Invalid key type: %s" % (err)
            self.add_error('sshPublicKey', message)
            return False

        if ssh.key_type.decode() not in settings.NACHO_SSH_KEYTYPES:
            message = 'Keytype must be one of %s.' % \
                ', '.join(settings.NACHO_SSH_KEYTYPES)
            self.add_error('sshPublicKey', message)
            return False

        self.cleaned_data['sshPublicKey'] = sshPublicKey

        return True


class LdapUserRemSshKeyForm(forms.Form):
    sshPublicKey = forms.CharField(
        widget=forms.Textarea(attrs={'readonly': 'readonly'}))


class LdapUserEmailUpdate(forms.Form):
    mail = forms.EmailField(label="New email address")

    def save(self, username, currentsite):
        mail = self.cleaned_data['mail']
        try:
            tmpldapuser = LdapTemporaryUser.objects.get(username=username)
        except LdapTemporaryUser.DoesNotExist:
            tmpldapuser = LdapTemporaryUser(username=username,
                                            cn="TmpValue",
                                            sn="TmpValue")
        try:
            ldapuser = LdapUser.objects.get(username=username)
            tmpldapuser.mail = mail
            tmpldapuser.save()
            subject = 'Change Your Emailaddress'
            uidb64 = urlsafe_base64_encode(force_bytes(username)).decode()
            message = render_to_string('email-templates/email_update.html', {
                'user': username, 'domain': currentsite,
                'username': uidb64,
                'token': email_update_token.make_token(ldapuser), })
            send_mail(subject, message,
                      settings.DEFAULT_FROM_EMAIL,
                      [tmpldapuser.mail])
        except LdapUser.DoesNotExist:
            pass


class LdapAccountDeletionRequestForm(forms.Form):
    username = forms.CharField(label='Confirm your username', max_length=100)

    def is_valid(self, username):
        valid = super(LdapAccountDeletionRequestForm, self).is_valid()
        if not valid:
            return valid

        if self.cleaned_data.get('username') != username:
            self.add_error('username', "Username is not valid")
            return False
        return True

    def save(self, currentsite):
        username = self.cleaned_data.get('username')
        try:
            user = LdapUser.objects.get(username=username)
            subject = 'Delete Your Account'
            uidb64 = urlsafe_base64_encode(force_bytes(username)).decode()
            message = render_to_string('email-templates/delete_account.html', {
                'user': username, 'domain': currentsite,
                'username': uidb64,
                'token': nacho_password_reset_token.make_token(user), })
            send_mail(subject, message,
                      settings.DEFAULT_FROM_EMAIL,
                      [user.mail])
        except LdapUser.DoesNotExist:
            pass
