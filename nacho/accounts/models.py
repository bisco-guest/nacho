# Copyright 2018 Nacho Developers
# See the COPYRIGHT file at the top-level directory of this distribution
#
# This file is part of Nacho. It is subject to the license terms in the LICENSE
# file found in the top-level directory of this distribution. No part of Nacho,
# including this file, may be copied, modified, propagated, or distributed
# except according to the terms contained in the LICENSE file.
#


import ldap
import logging

from django.conf import settings
from django.core.validators import validate_email

import ldapdb.models
from ldapdb.models.fields import CharField, IntegerField, ListField, ImageField

logger = logging.getLogger(__name__)


def validate_sn(val):
    return  # TODO


def validate_cn(val):
    return  # TODO


class __BaseUserClass(object):
    """ a parent class for the user functions """

    def set_password(self, raw_password):
        ldapcon = ldap.initialize(settings.AUTH_LDAP_SERVER_URI)
        try:
            ldapcon.simple_bind_s(
                settings.DATABASES['ldap']['USER'],
                settings.DATABASES['ldap']['PASSWORD'])
            ldapcon.passwd_s(self.dn, None, raw_password)
            return True
        except ldap.LDAPError as e:
            logger.error("Setpasswd(): " + str(e))
        return False

    def change_password(self, old_password, new_password):
        ldapcon = ldap.initialize(settings.AUTH_LDAP_SERVER_URI)
        try:
            ldapcon.simple_bind_s(self.dn, old_password)
            ldapcon.passwd_s(self.dn, old_password, new_password)
            return True
        except ldap.LDAPError as e:
            logger.error("Changepasswd(): " + str(e))
        return False

    def check_password(self, raw_password):
        ldapcon = ldap.initialize(settings.AUTH_LDAP_SERVER_URI)
        try:
            ldapcon.simple_bind_s(self.dn, raw_password)
            return True
        except ldap.LDAPError as e:
            logger.error("Checkpassword(): " + str(e))
        return False

    def __str__(self):
        return self.username

    def __unicode__(self):
        return self.username


class LdapUser(__BaseUserClass, ldapdb.models.Model):
    object_classes = ['shadowAccount', 'inetOrgPerson', 'ldapPublicKey']
    base_dn = "ou=people," + settings.NACHO_BASEDN

    # objectclass ( 2.5.6.6 NAME 'person'
    #     DESC 'RFC2256: a person'
    #     SUP top STRUCTURAL
    #     MUST ( sn $ cn )
    #     MAY ( userPassword $ telephoneNumber $ seeAlso $ description )
    # )

    sn = CharField(db_column=settings.NACHO_DBCOLSN, validators=[validate_sn])
    cn = CharField(db_column=settings.NACHO_DBCOLCN, validators=[validate_cn])
    # objectclass ( 2.5.6.7 NAME 'organizationalPerson'
    #     DESC 'RFC2256: an organizational person'
    #     SUP person STRUCTURAL
    #     MAY (
    #         title $ x121Address $ registeredAddress $ destinationIndicator $
    #         preferredDeliveryMethod $ telexNumber $
    #         teletexTerminalIdentifier $ telephoneNumber $
    #         internationaliSDNNumber $ facsimileTelephoneNumber $ street $
    #         postOfficeBox $ postalCode $ postalAddress $
    #         physicalDeliveryOfficeName $ ou $ st $ l
    #     )
    # )
    # attributes not used

    # objectclass ( 2.16.840.1.113730.3.2.2 NAME 'inetOrgPerson'
    #     DESC 'RFC2798: Internet Organizational Person'
    #     SUP organizationalPerson STRUCTURAL
    #     MAY (
    #         audio $ businessCategory $ carLicense $ departmentNumber $
    #         displayName $ employeeNumber $ employeeType $ givenName $
    #         homePhone $ homePostalAddress $ initials $ jpegPhoto $
    #         labeledURI $ mail $ manager $ mobile $ o $ pager $ photo $
    #         roomNumber $ secretary $ uid $ userCertificate $
    #         x500uniqueIdentifier $ preferredLanguage $
    #         userSMIMECertificate $ userPKCS12
    #     )
    # )
    # from organizationalPerson

    mail = CharField(db_column=settings.NACHO_DBCOLMAIL,
                     validators=[validate_email], null=True, blank=True)
    jpegPhoto = ImageField(db_column=settings.NACHO_DBCOLJPEGPHOTO,
                           validators=[], null=True, blank=True)
    # objectclass ( 1.3.6.1.1.1.2.1 NAME 'shadowAccount'
    #     DESC 'Additional attributes for shadow passwords'
    #     SUP top AUXILIARY
    #     MUST uid
    #     MAY (
    #         userPassword $ shadowLastChange $ shadowMin $
    #         shadowMax $ shadowWarning $ shadowInactive $
    #         shadowExpire $ shadowFlag $ description
    #     )
    # )

    # we rename the 'uid' field to 'username' to be a bit more conform with
    # the Django internals
    username = CharField(db_column=settings.NACHO_DBCOLUSERNAME,
                         validators=[], primary_key=True)
    userPassword = CharField(db_column='userPassword',
                             validators=[], null=True, blank=True)
    # all other attributes not used

    # objectclass ( 1.3.6.1.4.1.24552.500.1.1.2.0 NAME 'ldapPublicKey'
    #     DESC 'MANDATORY: OpenSSH LPK objectclass'
    #     SUP top AUXILIARY
    #     MUST uid
    #     MAY sshPublicKey
    # )
    # NOTE uid defined above

    sshPublicKey = ListField(db_column='sshPublicKey',
                             validators=[], null=True, blank=True)

    class Meta:
        managed = False


class LdapTemporaryUser(__BaseUserClass, ldapdb.models.Model):
    object_classes = ['shadowAccount', 'inetOrgPerson']
    base_dn = "ou=temp," + settings.NACHO_BASEDN
    USERNAME_FIELD = 'username'

    # objectclass ( 2.5.6.6 NAME 'person'
    #     DESC 'RFC2256: a person'
    #     SUP top STRUCTURAL
    #     MUST ( sn $ cn )
    #     MAY ( userPassword $ telephoneNumber $ seeAlso $ description )
    # )

    sn = CharField(db_column=settings.NACHO_DBCOLSN, validators=[validate_sn])
    cn = CharField(db_column=settings.NACHO_DBCOLCN, validators=[validate_cn])
    # objectclass ( 2.5.6.7 NAME 'organizationalPerson'
    #     DESC 'RFC2256: an organizational person'
    #     SUP person STRUCTURAL
    #     MAY (
    #         title $ x121Address $ registeredAddress $ destinationIndicator $
    #         preferredDeliveryMethod $ telexNumber $
    #         teletexTerminalIdentifier $ telephoneNumber $
    #         internationaliSDNNumber $ facsimileTelephoneNumber $ street $
    #         postOfficeBox $ postalCode $ postalAddress $
    #         physicalDeliveryOfficeName $ ou $ st $ l
    #     )
    # )
    # attributes not used

    # objectclass ( 2.16.840.1.113730.3.2.2 NAME 'inetOrgPerson'
    #     DESC 'RFC2798: Internet Organizational Person'
    #     SUP organizationalPerson STRUCTURAL
    #     MAY (
    #         audio $ businessCategory $ carLicense $ departmentNumber $
    #         displayName $ employeeNumber $ employeeType $ givenName $
    #         homePhone $ homePostalAddress $ initials $ jpegPhoto $
    #         labeledURI $ mail $ manager $ mobile $ o $ pager $ photo $
    #         roomNumber $ secretary $ uid $ userCertificate $
    #         x500uniqueIdentifier $ preferredLanguage $
    #         userSMIMECertificate $ userPKCS12
    #     )
    # )
    # from organizationalPerson

    mail = CharField(db_column=settings.NACHO_DBCOLMAIL,
                     validators=[validate_email], null=True, blank=True)
    jpegPhoto = ImageField(db_column=settings.NACHO_DBCOLJPEGPHOTO,
                           validators=[], null=True, blank=True)
    # objectclass ( 1.3.6.1.1.1.2.1 NAME 'shadowAccount'
    #     DESC 'Additional attributes for shadow passwords'
    #     SUP top AUXILIARY
    #     MUST uid
    #     MAY (
    #         userPassword $ shadowLastChange $ shadowMin $
    #         shadowMax $ shadowWarning $ shadowInactive $
    #         shadowExpire $ shadowFlag $ description
    #     )
    # )

    # we rename the 'uid' field to 'username' to be a bit more conform with
    # the Django internals
    username = CharField(db_column=settings.NACHO_DBCOLUSERNAME,
                         validators=[], primary_key=True)
    userPassword = CharField(db_column='userPassword',
                             validators=[], null=True, blank=True)
    # all other attributes not used

    class Meta:
        managed = False


class LdapGroup(ldapdb.models.Model):
    # objectclass ( 1.3.6.1.1.1.2.2 NAME 'posixGroup'
    #       SUP top STRUCTURAL
    #       DESC 'Abstraction of a group of accounts'
    #       MUST ( cn $ gidNumber )
    #       MAY ( userPassword $ memberUid $ description ) )
    base_dn = "ou=groups," + settings.NACHO_BASEDN
    object_classes = ['posixGroup']

    class Meta:
        managed = False

    gidNumber = IntegerField(db_column='gidNumber',
                             validators=[], null=True, blank=True, unique=True)
    cn = CharField(db_column='cn', validators=[], primary_key=True)
    memberUid = ListField(db_column='memberUid',
                          validators=[], null=True, blank=True)

    def __str__(self):
        return self.cn

    def __unicode__(self):
        return self.cn
